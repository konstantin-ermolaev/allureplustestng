package org.example;


import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;
import static settings.LiB.SET1;
import static settings.LiB.SET2;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test(groups = {"SET1"})
    public void shouldAnswerWithTrueOne()
    {
        assertTrue( true );
        System.out.println("test 1 passed");
    }

    @Test(groups = {SET2})
    public void shouldAnswerWithTrueTwo()
    {
        assertTrue( true );
        System.out.println("test 2 passed");
    }

    @Test(groups = {SET1,SET2})
    public void shouldAnswerWithTrueThree()
    {
        assertTrue( true );
        System.out.println("test 3 passed");
    }

    @Test
    public void shouldAnswerWithTrueFour()
    {
        assertTrue( true );
        System.out.println("test 4 passed");
    }
}
