package org.example;

public class Book {
    private int id;
    private String name;
    private String author;

    public Book() {
    }

    public Book (String author, String name, int id) {
        this.author = author;
        this.name = name;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }
}
